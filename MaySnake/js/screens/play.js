/*
 * Direction enum
 */
var DIRECTION = 
{
	RIGHT : 0,
	LEFT  : 1,
	UP    : 2,
	DOWN  : 3
};

game.PlayScreen = me.ScreenObject.extend({
    onResetEvent: function()
    {
	    // stuff to reset on state change

		//background color layer to clear the screen
//		me.game.add(new me.ColorLayer("background", "#224488", 0), 0);
		me.game.add(new me.ColorLayer("background", "#222222", 0), 0);
		me.game.add(new BackgroundObject());

		var player = me.entityPool.newInstanceOf("playerEntity", '1', 160, 80, DIRECTION.RIGHT);
		me.game.add(player, 11);
		
//		var food = new FoodEntity(160, 160);
//		me.game.add(food, 1);
		me.gamestat.add("players", 1);
//		var player2 = me.entityPool.newInstanceOf("playerEntity", '2', 640, 288, DIRECTION.RIGHT);
//		me.game.add(player2, 2);
//		player.init(50,50);
//		me.entityPool.add(player, 10);
		
		me.game.add(new FoodFactory(), 1);
		TARGET_TICKS = 4;

		me.audio.play("start");
	    me.game.sort();
	},

	/* ---
	
	action to perform when game is finished (state change)
	
	--- */
	onDestroyEvent: function() {
		//disable the HUD for safety!
	}
});
