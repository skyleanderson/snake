//game resources
game.resources = [
    /**
     * Graphics.
     */
	//play screen related
    {name: "snake_head",     type:"image", src: "data/img/sprites/snake_head.png"},
    {name: "snake_square",   type:"image", src: "data/img/sprites/snake_square.png"},
    {name: "food",           type:"image", src: "data/img/sprites/food.png"},
	
 
    //title screen
    {name: "title",           type:"image", src: "data/img/gui/title.png"},
    {name: "play",            type:"image", src: "data/img/gui/play.png"},
    {name: "play_hover",      type:"image", src: "data/img/gui/play_hover.png"},
    {name: "mute",            type:"image", src: "data/img/gui/mute.png"},
    {name: "mute_hover",      type:"image", src: "data/img/gui/mute_hover.png"},
    {name: "unmute",          type:"image", src: "data/img/gui/unmute.png"},
    {name: "unmute_hover",    type:"image", src: "data/img/gui/unmute_hover.png"},
    
    //backgrounds
    {name: "bg1",            type:"image", src: "data/img/gui/bg1.png"},
    {name: "bg2",            type:"image", src: "data/img/gui/bg2.png"},
    
    //sounds
    {name: "start",          type: "audio", src: "data/sfx/", channel : 1},
    {name: "pickup",         type: "audio", src: "data/sfx/", channel : 1},
    {name: "death",          type: "audio", src: "data/sfx/", channel : 1}

];
