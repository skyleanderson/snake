/*
* menu screen
*/

var TitleScreen = me.ScreenObject.extend(
{
	/*
	* constructor
	*/
	init: function()
	{
		// call parent constructor
		this.parent(true, true);
		
		// init stuff
		this.title = null;
		this.play = null;
		this.version = null;
		me.gamestat.add("highscore");
		me.gamestat.setValue("highscore", -1);
		me.gamestat.add("muted");
		me.gamestat.setValue("muted", 0);
	},
	
	
	/*
	* reset function
	*/
	onResetEvent: function()
	{
		me.game.add(new me.ColorLayer("background", "#222222", 0), 0);
		me.game.add(new BackgroundObject());
		
		// load title image
		this.title = me.loader.getImage("title");
		
		// play button
		this.play = new Button("play", me.state.PLAY, -1, 220);
		this.mute = new MuteButton(me.video.getWidth() - 35, me.video.getHeight() - 35);
		
		// version
		this.scoreFont = new me.Font("Lucida Console", 18, "#1c7100");
		me.game.sort();
 	    me.input.bindKey(me.input.KEY.ENTER,  "go");
	},
	
		/**
		 * update function
		 */
	update: function()
	{
			 this.parent();
			 if (me.input.isKeyPressed("go")) 
			 {
				 me.audio.play("start");
				 me.state.change(me.state.PLAY);
			 }
	},
		 
	/*
	* drawing function
	*/
	draw: function(context)
	{
		// draw title
		context.drawImage(this.title, (me.video.getWidth() / 2 - this.title.width / 2), 20);
		
		// draw play button
		this.play.draw(context);
		
		this.mute.draw(context);

		//draw highscore
		if (me.gamestat.getItemValue("highscore") >= 0)
		{
			var text = "Highscore: " + me.gamestat.getItemValue("highscore");
			var scoreWidth = this.scoreFont.measureText(context, text).width;
			var scorex = (me.video.getWidth() / 2 + this.title.width / 2) - scoreWidth - 5;
			this.scoreFont.draw(context, text, scorex, 20 + this.title.height);
//			this.scoreFont.draw(context, text, 0, 0);
		}
	},
	
	/*
	* destroy event function
	*/
	onDestroyEvent: function()
	{
		// release mouse event
		me.input.releaseMouseEvent("mousedown", this.play);
	}
});

/*
* draw a button on screen. Borrowed from invasionJS by semche
*/
var Button = me.Rect.extend(
{
	/*
	* constructor
	*/
	init: function(image, action, x, y)
	{
		// init stuff
		this.image = me.loader.getImage(image);
		this.image_hover = me.loader.getImage(image + "_hover");
		this.action = action;
		
		//if x or y is -1, center on screen
		var endx = x >= 0 ? x : (me.video.getWidth()  / 2 - this.image.width / 2);
		var endy = y >= 0 ? y : (me.video.getHeight() / 2 - this.image.height / 2);
		
		this.pos = new me.Vector2d(endx, endy);
		
		// call parent constructor
		this.parent(this.pos, this.image.width, this.image.height);
		
		// register mouse event
		me.input.registerMouseEvent("mousedown", this, this.clicked.bind(this));
	},
	
	/*
	* action to perform when a button is clicked
	*/
	clicked: function()
	{
		// start action
		me.state.change(this.action);
	},
	
	/*
	* drawing function
	*/
	draw: function(context)
	{
		// on button hovered
		if (this.containsPoint(me.input.mouse.pos))
			context.drawImage(this.image_hover, this.pos.x, this.pos.y);
		else
			context.drawImage(this.image, this.pos.x, this.pos.y);
	},
	
	update: function()
	{
		this.parent();
		return true;
	},
	
	/*
	* destroy event function
	*/
	onDestroyEvent: function()
	{
		// release mouse events
		me.input.releaseMouseEvent("mousedown", this);
	}
});

/*
* draw a button on screen. Borrowed from invasionJS by semche
*/
var MuteButton = me.Rect.extend(
{
	/*
	* constructor
	*/
	init: function(x, y)
	{
		// init stuff
		this.muteImage        = me.loader.getImage("mute");
		this.muteImageHover   = me.loader.getImage("mute_hover");
		this.unmuteImage      = me.loader.getImage("unmute");
		this.unmuteImageHover = me.loader.getImage("unmute_hover");
		this.muted = me.gamestat.getItemValue("muted");
		
		this.pos = new me.Vector2d(x, y);
		
		// call parent constructor
		this.parent(this.pos, this.muteImage.width, this.muteImage.height);
		
		// register mouse event
		me.input.registerMouseEvent("mousedown", this, this.clicked.bind(this));
	},
	
	/*
	* action to perform when a button is clicked
	*/
	clicked: function()
	{
		// toggle states
		this.muted = !this.muted;
		me.gamestat.setValue("muted", this.muted);

		if (this.muted)
			me.audio.muteAll();
		else
			me.audio.unmuteAll();
	},
	
	/*
	* drawing function
	*/
	draw: function(context)
	{
		// on button hovered
		if (this.muted)
		{
			if (this.containsPoint(me.input.mouse.pos))
				context.drawImage(this.muteImageHover, this.pos.x, this.pos.y);
			else
				context.drawImage(this.muteImage, this.pos.x, this.pos.y);
		} else //not muted
		{
			if (this.containsPoint(me.input.mouse.pos))
				context.drawImage(this.unmuteImageHover, this.pos.x, this.pos.y);
			else
				context.drawImage(this.unmuteImage, this.pos.x, this.pos.y);
		}
	},
	
	update: function()
	{
		this.parent();
		return true;
	},
	
	/*
	* destroy event function
	*/
	onDestroyEvent: function()
	{
		// release mouse events
		me.input.releaseMouseEvent("mousedown", this);
	}
});
