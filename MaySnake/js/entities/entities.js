/*
 * Direction enum
 */
var DIRECTION = 
{
	RIGHT : 0,
	LEFT  : 1,
	UP    : 2,
	DOWN  : 3
};

/*------------------- 
a player entity
-------------------------------- */
var TARGET_TICKS = 4;
var PlayerEntity = me.ObjectEntity.extend({
 
    /* -----
 
    constructor
 
    ------ */
	//closest to player
	head: null,
	//farthest from player
    tail: null,
 
    init: function(id, x, y, startDirection) {

		var settings = {};
        settings.image = "snake_head";
        settings.spritewidth = 16;
        settings.spriteheight = 16;

        // call the constructor
        this.parent(x, y, settings);
 
		TARGET_TICKS = 4;
        this.id = id;
        this.leftButton = "left" + id;
        this.rightButton = "right" + id;
        this.upButton = "up" + id;
        this.downButton = "down" + id;
        
        //add animations for horizontal or vertical
        this.renderable.addAnimation("hori", [0]);
        this.renderable.addAnimation("vert", [1]);
        this.renderable.addAnimation("dead", [2]);
        
        this.renderable.alwaysUpdate = true;

        // set the default horizontal & vertical speed (accel vector)
        var velo = this.width / TARGET_TICKS;
        this.setVelocity(velo, velo);

        this.collidable = false;
        
        //this will be the first of a linked list of SnakeLinkEntity objects
        this.head = null;
        
        var node = null;
        var prevNode = null;
        var offset = 0;
    	this.head = me.entityPool.newInstanceOf("snakeLinkEntity", this.pos.x - 16, this.pos.y);
    	this.head.prev = null;
		me.game.add(this.head, 1);
		prevNode = this.head;
		for (var i=1; i<3; i++) //start with 3 links
        {
        	offset = 16 * (i+1);
        	switch (startDirection)
        	{
        	case DIRECTION.LEFT:
	        	node = me.entityPool.newInstanceOf("snakeLinkEntity", this.pos.x + offset, this.pos.y);
	    		break;
        	case DIRECTION.RIGHT:
	        	node = me.entityPool.newInstanceOf("snakeLinkEntity", this.pos.x - offset, this.pos.y);
	    		break;
        	case DIRECTION.UP:
	        	node = me.entityPool.newInstanceOf("snakeLinkEntity", this.pos.x, this.pos.y + offset);
	        	break;
        	case DIRECTION.DOWN:
	        	node = me.entityPool.newInstanceOf("snakeLinkEntity", this.pos.x, this.pos.y - offset);
	    		break;
        	}
    		me.game.add(node, 10);
    		node.prev = prevNode;
    		prevNode.next = node;
    		prevNode = node;
    		if (i == 2) //save the last one in the list
    			this.tail = node;
        }
	    this.ticksSinceLastUpdate = 0;

        //declare a direction variable
        this.currentDirection = DIRECTION.RIGHT;
        this.nextDirection = DIRECTION.RIGHT;
        this.renderable.setCurrentAnimation("hori");
        
        this.grow = 7;

        this.alive = true;
        me.gamestat.add("food", 0);
    },
 
    /* -----
 
    update the player pos
 
    ------ */
    update: function() {

		// check & update player movement
        this.updateMovement();
        
        //update direction based on input
        if (me.input.isKeyPressed(this.leftButton) && this.currentDirection != DIRECTION.RIGHT) 
            this.nextDirection = DIRECTION.LEFT;
        else if (me.input.isKeyPressed(this.rightButton)  && this.currentDirection != DIRECTION.LEFT) 
            this.nextDirection = DIRECTION.RIGHT;
        else if (me.input.isKeyPressed(this.downButton) && this.currentDirection != DIRECTION.UP)
            this.nextDirection = DIRECTION.DOWN;
        else if (me.input.isKeyPressed(this.upButton)  && this.currentDirection != DIRECTION.DOWN)
            this.nextDirection = DIRECTION.UP;

    	this.ticksSinceLastUpdate += me.timer.tick;
		if (this.ticksSinceLastUpdate >= TARGET_TICKS)
		{
			this.ticksSinceLastUpdate = 0;

			//turn based on next direction
	        if (this.nextDirection == DIRECTION.LEFT) 
	        {
	            // flip the sprite on horizontal axis
	            this.flipX(true);
	            // update the entity velocity
	            this.vel.x = -this.accel.x * me.timer.tick;
	            this.vel.y = 0;
	        } 
	        else if (this.nextDirection == DIRECTION.RIGHT) 
	        {
	            // unflip the sprite
	            this.flipX(false);
	            // update the entity velocity
	            this.vel.x = this.accel.x * me.timer.tick;
	            this.vel.y = 0;
	        } 
	        else if (this.nextDirection == DIRECTION.DOWN)
	        {
	        	this.vel.y = this.accel.y * me.timer.tick;
	            this.vel.x = 0;
	            this.flipY(true);
	        } 
	        else if (this.nextDirection == DIRECTION.UP)
	        {
	        	this.vel.y = -this.accel.y * me.timer.tick;
	            this.vel.x = 0;
	            this.flipY(false);
	        }
	        this.currentDirection = this.nextDirection;
	        
	        if (me.input.isKeyPressed("debug"))
	        	alert(me.timer.tick);
	 
	        	
	        if (!this.alive)
	        	this.renderable.setCurrentAnimation("dead");
	        	else
	        {
		        if (this.currentDirection == DIRECTION.RIGHT || this.currentDirection == DIRECTION.LEFT)
		        	this.renderable.setCurrentAnimation("hori");
		    	else
		        	this.renderable.setCurrentAnimation("vert");
	        }
	        	
	        this.updateTail();
		}
		
        
		if (this.alive &&
			this.pos.x < 0 || this.pos.x + this.width > me.video.getWidth() ||
			this.pos.y < 0 || this.pos.y + this.height > me.video.getHeight())
		{
			//go back and kill yourself...
			this.pos.x = this.head.pos.x;
			this.pos.y = this.head.pos.y;
			this.alive = false;
    		// DEATH
			me.audio.play("death");
			TARGET_TICKS = 1;

			return true;
		}

        //collisions
		var res = null;
        if (this.head != null)
        	res = me.game.collide(this.head);
        
        if (this.alive && res) //collision occurred
        {
        	if (res.obj.type == me.game.COLLECTABLE_OBJECT)
        		this.grow += 8;
        	else
        	{
        		// DEATH
    			me.audio.play("death");

        	   	this.alive = false;
        	   	TARGET_TICKS = 1;
        	}
        	   	
//        	
//        	if (this.grow == 0)
//            	alert("Collision with node: " + res.obj.pos.x + ", " + res.obj.pos.y);
//
//        	if (res.obj !== this.head) //make sure the collision isn't planned
//        	{
//        		this.alive = false;
//        	}
        }

 
        this.parent();
        return true;

    },
    
    updateMovement: function()
    {
    	if (this.alive)
    	{
	    	this.pos.x  += this.vel.x;
	    	this.pos.y  += this.vel.y;
    	}
    },
    
    /**
     * makes the tail follow the head position
     */
    updateTail: function()
    {
    	if (this.head != null)
    	{
    		if (this.grow == 0)
    		{
    			if (this.alive)
    			{
			        //update the tail at the appropriate intervals
	    			//NEW WAY, swap the tail to the new position
					var current = this.tail;
					this.tail = current.prev; //update the tail
					this.tail.next = null; //detach from the end of the list
					
					this.head.prev = current; //double link
					current.next = this.head; //make the head #2 in list
					this.head = current;      //move the current to head
					this.head.prev = null;
					
					//update the new head's position
					current.pos.x = this.pos.x;
					current.pos.y = this.pos.y;
    			}
    			else
				{
					//remove one link from the tail
					current = this.tail;
					if (current.prev != null)
					{
						this.tail = current.prev;
						this.tail.next = null;
						me.game.remove(current);
						current = null;
					}else
					{
						//no more nodes left
						me.game.remove(this.head);
						this.tail = null;
						this.head = null;
					}
					

				}

		        //OLD WAY::
//				var current = this.head;
//				var next = null;
//					
//				//assume head has been set properly..
//				var nextX = current.pos.x;
//				var nextY = current.pos.y;
//				var currX = current.pos.x;
//				var currY = current.pos.y;
//				current.pos.x = this.pos.x;
//				current.pos.y = this.pos.y;
//					
//				//loop through the list, assume current is updated, update the next in list until done
//				while (current != null) 
//				{
//					next = current.next;
//					//move up to the next link
//					if (next != null) //follow the leader
//					{
//					    nextX = next.pos.x;
//					    nextY = next.pos.y;
//						next.pos.x = currX;
//						next.pos.y = currY;
//						currX = nextX;
//						currY = nextY;
//					} 
//					current = next;
//				}
//				if (!this.alive)
//				{
//					//remove one link from the tail
//					current = this.head;
//					this.head = current.next;
//					me.game.remove(current);
//					current = null;
//				}
			} else // if grow > 0
    		{
    			//don't update all the links, just add a new one to the front
				var node = me.entityPool.newInstanceOf("snakeLinkEntity", this.pos.x, this.pos.y);
				me.game.add(node, 1);
				me.game.sort();
				this.head.prev = node;
				node.next = this.head;
				node.prev = null;
				this.head = node;
				this.grow--;
    		}
		} else
    	{
    		//no tail, remove the player
			if (me.gamestat.getItemValue("food") > me.gamestat.getItemValue("highscore"))
				me.gamestat.setValue("highscore", me.gamestat.getItemValue("food"));
				
			me.game.remove(this);
			me.state.change(me.state.TITLE);
    	}
    	
    }

});

var SnakeLinkEntity = me.ObjectEntity.extend({

	next: null,
	prev: null,

	init: function(x, y) 
	{
		var settings = {};
	    settings.image = "snake_square";
	    settings.spritewidth = 16;
	    settings.spriteheight = 16;
	
	    // call the constructor
	    this.parent(x, y, settings);
	
	    //add animations for horizontal or vertical
	    this.renderable.addAnimation("hori", [0]);
	    this.renderable.addAnimation("vert", [1]);
	
	    // set the default horizontal & vertical speed (accel vector)
	    this.setVelocity(0,0); //never move
	
	    this.collidable = true;
	    
	    //declare a direction variable
	    this.currentDirection = DIRECTION.RIGHT;
	    this.next = null; //next link in the snake
	    this.head = null; //head that player is controlling
	    this.renderable.setCurrentAnimation("hori");
	},

	update: function() 
	{
		/// no need for anything to happen, really..
		return false;
	}
}
);

/*----------------
 a Food entity
------------------------ */
var FOOD_LIFE = 600; //10 seconds
var FoodEntity = me.CollectableEntity.extend(
{
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function(x, y) 
    {
        // call the parent constructor
		var settings = {};
	    settings.image = "food";
	    settings.spritewidth = 16;
	    settings.spriteheight = 16;

        this.parent(x, y, settings);
        this.life = FOOD_LIFE;
    },
    
    update: function()
    {
    	this.life -= me.timer.tick;
    	if (this.life <= 0)
    	{
    		this.collidable = false;
    		me.gamestat.updateValue("ActiveFood", -1);
    		me.game.remove(this);
    	}
    	return false;
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
    onCollision: function() 
    {
        // do something when collected
    	me.audio.play("pickup");
        // make sure it cannot be collected "again"
        this.collidable = false;
        // remove it
        me.gamestat.updateValue("ActiveFood", -1);
        me.gamestat.updateValue("food", 1);
        me.game.remove(this);
    }
 
}
);


/*----------------
 a Food generator, make sure food shows up regurlarly
------------------------ */
var FoodFactory = me.ObjectEntity.extend(
{
    // extending the init function is not mandatory
    // unless you need to add some extra initialization
    init: function() 
    {
		var settings = {};
	    settings.image = "food";
	    settings.spritewidth = 1;
	    settings.spriteheight = 1;
    	this.parent(0,0,settings);
    	this.round = 0;
    	me.gamestat.add("ActiveFood", 0);
    	this.collidable = 0;
    	this.renderable.alwaysUpdate = true;
    },
 
   update: function()
   {
	   if (me.gamestat.getItemValue("ActiveFood") == 0)
	   {
		   //spawn food, update round
		   this.round++;
		   
		   var foodCount = 0;
		   var playerCount = me.gamestat.getItemValue("players");
		   if (this.round < 10)
			   foodCount = playerCount;
		   else
			   foodCount = (playerCount * 2) ;
		
			   
		   var pellet = null;
		   var width = me.video.getWidth() / 16;
		   var height = me.video.getHeight() / 16;
		   var x;
		   var y;
		   for (var i=0; i<foodCount; i++)
		   {
			   x = Number.prototype.random(0, width-1) * 16;
			   y = Number.prototype.random(0, height-1) * 16;
		       pellet = me.entityPool.newInstanceOf("food", x, y);
		       me.game.add(pellet, 12);
		   }
		   me.gamestat.updateValue("ActiveFood", foodCount);
		   me.game.sort();
		   return false;
	   }
   }
 
}
);

/*******************************
 * GUI STUFF, borrowed from invasionJS
 * ******************************/
	
/*
* background layer
*/
var BackgroundLayer = me.ImageLayer.extend(
{
	/*
	* constructor
	*/
	init: function(image, speed, vert)
	{
		this.name = image;
		if (vert)
		{
			this.width = me.video.getWidth();
			this.height = 2 * me.video.getHeight();
		}
		else
		{
			this.width = 2 * me.video.getWidth();
			this.height = me.video.getHeight();
		}
		z = 1;
		ratio = 1;
		
		// call parent constructor
		this.parent(this.name, this.width, this.height, image, z, ratio);
		this.speed = speed;
		this.vert = vert; //boolean
		this.setOpacity(.08);
	},
	
	/*
	* update function
	*/
	update: function()
	{
		if (this.vert)
		{
			
			// increment vert background position
			this.translate(0, this.speed);

			// recalibrate image position
			if (this.pos.y >= this.height)
			{
				this.pos.y = this.height;
				this.speed *= -1;
			} else if (this.pos.y <= 0)
			{
				this.pos.y = 0
				this.speed *= -1;
			}
		}
		else
		{
			
			// increment horizontal background position
			this.translate(this.speed, 0);
			// recalibrate image position
			if (this.pos.x >= this.width)
			{
				this.pos.x = this.width;
				this.speed *= -1;
			} else if (this.pos.x <= 0)
			{
				this.pos.x = 0
				this.speed *= -1;
			}
		}
		return true;
	}
});

/*
* parallax background
*/
var BackgroundObject = me.ObjectEntity.extend(
{
	/*
	* constructor
	*/
	init: function()
	{
		me.game.add(new BackgroundLayer("bg1", 0.28, false), 2); // layer 1
		me.game.add(new BackgroundLayer("bg2", 0.38, true), 1); // layer 2
		me.game.sort();
	},
	
	/*
	* update function
	*/
	update: function()
	{
		return true;
	}
});
 

