
/* Game namespace */
var game = {
    // Run on page load.
    "onload" : function () {
        // Initialize the video.
//        if (!me.video.init("screen", 1280, 720, true, 'auto', true)) {
        if (!me.video.init("screen", 640, 352, true, 'auto', true)) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }
		
		// add "#debug" to the URL to enable the debug Panel
		if (document.location.hash === "#debug") {
			window.onReady(function () {
				me.plugin.register.defer(debugPanel, "debug");
			});
		}

        // Initialize the audio.
        me.audio.init("mp3, ogg"); 

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
     
        // Load the resources.
        me.loader.preload(game.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },



    // Run on game resources loaded.
    "loaded" : function () 
    {
    	
    	//turn off gravity for snake
    	me.sys.gravity = 0;
 	   
		//add players to the entity pool
		me.entityPool.add("playerEntity", PlayerEntity);
		//add players to the entity pool
		me.entityPool.add("snakeLinkEntity", SnakeLinkEntity);
		me.entityPool.add("food", FoodEntity);

	   // set the "Play/Ingame" Screen Object
	   me.state.set(me.state.PLAY, new game.PlayScreen());
	   me.state.set(me.state.TITLE, new TitleScreen());

	   // enable the keyboard
	   me.input.bindKey(me.input.KEY.LEFT,   "left1");
	   me.input.bindKey(me.input.KEY.RIGHT,  "right1");
	   me.input.bindKey(me.input.KEY.UP,     "up1");
	   me.input.bindKey(me.input.KEY.DOWN,   "down1");
	   
	   me.input.bindKey(me.input.KEY.A,  "left2");
	   me.input.bindKey(me.input.KEY.D,  "right2");
	   me.input.bindKey(me.input.KEY.W,  "up2");
	   me.input.bindKey(me.input.KEY.S,  "down2");
	   
//	   me.input.bindKey(me.input.KEY.D, "debug");
      
	   // start the game 
	   me.state.change(me.state.TITLE);
    }
};
